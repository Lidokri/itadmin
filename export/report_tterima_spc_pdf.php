<?php
    session_start();
    include "../koneksi.php";
	include ("../ref_fun.php");
    require('fpdf/fpdf.php');
    $kdspc='%';
	//echo '  Pencarian: '.$idbr;
     
    //$query ="select * from master_perangkat";
	$query = "select a.*,b.unit, get_nmunit (b.unit) nama_unit, b.spek1, b.qty
              from tran_spc_induk a, tran_spc_detail b
              where a.kode_spc =b.kode_spc  and
              a.kode_spc like '%$_GET[pilisi]%' ";

    $query1 = "select * from tran_spc_induk ";
    $result = mysql_query($query1);
	$baris=1; //menambahkan variabel baris
    //Variabel untuk iterasi
    $i = 0;
	$tinggi=0.5;
    //Mengambil nilai dari query database
    while($data=mysql_fetch_row($result))
    {
		$cell[$i][0] = $data[0];
        $cell[$i][1] = $data[1];
		$cell[$i][2] = $data[2];
        $cell[$i][3] = $data[3];
        $cell[$i][4] = $data[5];
        $cell[$i][5] = $data[6];
		$cell[$i][6] = $data[6];
		$cell[$i][7] = $data[7];
		$cell[$i][8] = $data[11];
		$cell[$i][9] = $data[10];
		$cell[$i][10] = $data[11];
		$cell[$i][11] = $data[12];
		$cell[$i][12] = $data[13];
		$cell[$i][13] = $data[14];
		$cell[$i][14] = $data[14];
        $i++;
    }
    //memulai pengaturan output PDF
    class PDF extends FPDF
    {
        //untuk pengaturan header halaman
        function Header()
        {
            //Pengaturan Font Header
            $this->SetFont('Times','B',14); //jenis font : Times New Romans, Bold, ukuran 14
            //untuk warna background Header
            $this->SetFillColor(255,255,255);
            //untuk warna text
            $this->SetTextColor(0,0,0);
            //Menampilkan tulisan di halaman
            $this->Cell(28,1,'BERITA ACARA SERAH TERIMA HARDWARE (SPC)','B',0,'C',1); 
			//TBLR (untuk garis)=> B = Bottom, L = Left, R = Right, untuk garis, C = center
			$this->SetFont('Arial','B',7.5); //jenis font : Times New Romans, Bold, ukuran 14
			$this->Ln(1);
			$this->image('images/logo_aal.png',1,1,1.5,1);
			//$this->Image('logo.png',10,6,30);
			
           //Ln() = untuk pindah baris
           //$pdf->SetFont('Times','B',12);
//		$pdf->MultiCell(2.1,0.5,$cell[$j][0],'LBTR',0,'C');
 		     $this->Cell(1,0.5,'ID SPC:','LTB',0,'C');
//  		     $pdf->Cell(1,0.5,$cell[$i][0],'BT',0,'L');

             $this->Cell(2,0.5,'TGL SPC','RTB',0,'C');
			 $this->Ln();
	         $this->Cell(2,0.5,'ID DIVISI','LRTB',0,'C');
			 $this->Cell(2,0.5,'KET. USER','LRTB',0,'C');
             $this->Cell(1.5,0.5,'KET. NPK','LRTB',0,'C');
             $this->Cell(3,0.5,'KET. JML.PERSON','LRTB',0,'C');
             $this->Cell(3,0.5,'JML PC/PRINTER','LRTB',0,'C');
	         $this->Cell(2.5,0.5,'KET. SPEK LAMA','LRTB',0,'C');
	         $this->Cell(2.5,0.5,'URAIAN ALASAN','LRTB',0,'C');
			 $this->Cell(2.5,0.5,'REKOMENDASI GA','LRTB',0,'C');
			 $this->Cell(2,0.5,'LAMP. NAMA','LRTB',0,'C');
			 $this->Cell(1.5,0.5,'LAMP. NPK','LRTB',0,'C');
			 $this->Cell(2,0.5,'LAPM. DEPT','LRTB',0,'C');
			 $this->Cell(1.5,0.5,'STS','LRTB',0,'C');
			 $this->Ln();
			
        }
    function Footer()
    {
    // Posisi 15 cm dari bawah
    $this->SetY(-6);
    // Arial italic 8
    $this->SetFont('Arial','',8);
    // Page number
    $this->Cell(0,10,'Hal. '.$this->PageNo().'/{nb}',0,0,'C');
    }

}
// Page footer
	
    //pengaturan ukuran kertas P = Portrait
    $pdf = new PDF('L','cm','A4');
	$pdf->SetMargins(0.5,1,0);
    $pdf->Open();
          //Alias total halaman dengan default {nb} (berhubungan dengan PageNo())
     $pdf->AliasNbPages();
     $pdf->AddPage();
   
    $pdf->SetFont('Times',"",7);
	//$pdf->SetHeight(0.1);
   $pdf->Cell(1,0.5,'ID SPC:','LTB',0,'C');
   $pdf->Cell(1,0.5,$data[0],'BT',0,'L');
  

  for($j=0;$j<$i;$j++)
    {
        //menampilkan data dari hasil query database
        //$pdf->Cell(3,1,$j+1,'LBTR',0,'C');
//		$pdf->MultiCell(2.1,0.5,$cell[$j][0],'LBTR',0,'C');
		$pdf->Cell(1,0.5,$cell[$j][0],'LBTR',0,'L');
        $pdf->Cell(2,0.5,$cell[$j][1],'LBTR',0,'L');
		$pdf->Cell(2,0.5,$cell[$j][3],'LBTR',0,'L');
        $pdf->Cell(2,0.5,$cell[$j][5],'LBTR',0,'L');
        $pdf->Cell(1.5,0.5,$cell[$j][6],'LBTR',0,'L');
        $pdf->Cell(3,0.5,$cell[$j][6],'LBTR',0,'L');
		$pdf->Cell(3,0.5,$cell[$j][8],'LBTR',0,'L');
		$pdf->Cell(2.5,0.5,$cell[$j][11],'LBTR',0,'L');
		$pdf->Cell(2.5,0.5,$cell[$j][9],'LBTR',0,'C');
		$pdf->Cell(2.5,0.5,$cell[$j][10],'LBTR',0,'L');
		$pdf->Cell(2,0.5,$cell[$j][11],'LBTR',0,'C');
		$pdf->Cell(1.5,0.5,$cell[$j][12],'LBTR',0,'L');
		$pdf->Cell(2,0.5,$cell[$j][13],'LBTR',0,'C');
		$pdf->Cell(1.5,0.5,$cell[$j][14],'LBTR',0,'C');
        $pdf->Ln();
	}

    $pdf->Output();

	
?>