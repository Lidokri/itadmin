<html>
<?php
 // Define relative path from this script to mPDF
$nama_dokumen='Form-cetak_spc_verif'; //Beri nama file PDF hasil.
define('_MPDF_PATH','mpdf/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8', 'A4',-8,-5,-8,-5); // Create new mPDF Document
 
//Beginning Buffer to save PHP variables and HTML tags
//KONEKSI
include "../koneksi.php";
include ("../ref_fun.php");
$query2 = "select a.*, get_nmpt(a.pt) nmpt, get_area(a.pt) area, 
                  get_direktorat (a.dept,a.pt) nmdirektorat,
                  get_div(a.dept) nmdivisi, 
                  get_nmunit(a.rekomendasi) nmunit,  
                  get_nmspek1(a.rekomendasi) nmspek1,  
                  get_nmspek2(a.rekomendasi) nmspek2,  
                  get_harga(a.rekomendasi) harga,  
                  get_harga2(a.rekomendasi) harga2,  
                  get_nmpic(a.pic_approv1) nmapprov1,  
                  get_nmpic(a.pic_approv2) nmapprov2,  
                  get_nmpic(a.pic_approv3) nmapprov3,
                  get_jabpic(a.pic_approv1) nmjabatan1, 
                  get_jabpic(a.pic_approv2) nmjabatan2, 
                  get_jabpic(a.pic_approv3) nmjabatan3 
              from tran_spc_det_verifikasi a  where a.kode_spc like '$_GET[id]' ";
	$result = mysql_query($query2);
    $data2 = mysql_fetch_array($result);
$kol1=strlen($data2['nmapprov1']);
$kol2=strlen($data2['nmapprov3']);

ob_start();
?>
<head>
<style> 
#outln {
    margin-top: 0.5px;
    margin-bottom: 0.5px;
    margin-left: 0.5px;
    margin-right: 0.5px;
    outline: solid 1px;
    width: 1150px;
    height: 500px;
}
h3 {
    display: block;
    font-size: 14px;
    margin-top: 0.06em;
    margin-bottom: 0.06em;
    margin-left: 0;
    margin-right: 0;
}
h4 {
    display: block;
    font-size: 11px;
    margin-top: 0.06em;
    margin-bottom: 0.06em;
    margin-left: 0;
    margin-right: 0;
}
</style>
</head>
<?php //ob_start(); id="outln"  ?>

<body>
<div style='margin:0; font-size:11px; font-family:Arial; text-align: center;' >
<table rules='none' border='1' cellpadding='0.2' align='center'  width='1280px' height='1500px' >
<tr><td>
  <table border=0 width='1276px' style='margin:0;' >
  <tr>
    <td width='20%'><?php str_repeat('&nbsp; ',10); ?> </td>  <td width='80%'> </td>
  </tr>
  <tr>
    <td width='20%'>
    <img src="images/logo_aal.png" &nbsp; border="0" align="center" width="52" height="50" />
    </td>
    <td width='80%' style='text-align: center;' >
    <h3> HASIL VERIFKASI DAN REKOMENDASI KEBUTUHAN KOMPUTER </h3> 
	<h3> HEAD OFFICE / SITE  </h3> 
	<h2> PT. ASTRA AGRO LESTARI, Tbk.</h2>
    </td>
  </tr>
  <tr>
    <td width='20%'><?php str_repeat('&nbsp; ',10); ?>  </td>  <td width='80%'> </td>
  </tr>
  
  </table>
<hr />
<?php 
echo "<table border=0 width='1276px' style='margin:0; font-size:11px; font-family:Arial;' >";
echo "<tr width='40%'>
          <td>".str_repeat('&nbsp;',2)."<b>NO. VERIFIKASI".str_repeat('&nbsp;',5)." :</b>".$data2['no_verif']."</td>
          <td>".str_repeat('&nbsp;',2)."<b>TGL VERIFIKASI".str_repeat('&nbsp;',10)." :</b>".tgl_indo($data2['tgl_ver'])."</td>
     </tr>";
echo "<tr  width='40%'> <td>".str_repeat('&nbsp; ',10)." </td> <td>".str_repeat('&nbsp; ',10)." </td> </tr>";
echo "<tr>
          <td>".str_repeat('&nbsp;',2)."<b>NO. SPC".str_repeat('&nbsp;',18)." :</b>".$data2['kode_spc']." </td>
          <td>".str_repeat('&nbsp;',2)."<b>TGL REF.SPC ".str_repeat('&nbsp;',14).":</b>".tgl_indo($data2['tgl_ref_spc'])." </td>
     </tr>";
echo "<tr  width='40%'> <td>".str_repeat('&nbsp; ',10)." </td> <td>".str_repeat('&nbsp; ',10)." </td> </tr>";
echo "<tr>
          <td>".str_repeat('&nbsp;',2)."<b> NPK USER".str_repeat('&nbsp;',16).": </b>".$data2['npk_user']."</td>
          <td>".str_repeat('&nbsp;',2)."<b> TGL USER".str_repeat('&nbsp;',21).": </b>".tgl_indo($data2['tgl_user'])." </td>
     </tr>";
echo "<tr  width='40%'> <td>".str_repeat('&nbsp; ',10)." </td> <td>".str_repeat('&nbsp; ',10)." </td> </tr>";
echo "</table>";
echo "<p  style='margin:0; font-size:11px; font-family:Arial;'>";

echo str_repeat('&nbsp;',3)."<b>NAMA USER".str_repeat('&nbsp;',14).":<font style='text-align: left; background-color:Yellow;'>".$data2['nama_user']. "</font><br/>";
echo "<br/>".str_repeat('&nbsp;',3)."<b>DEPT/DIVISI".str_repeat('&nbsp;',15).":</b>".$data2['dept'].str_repeat('&nbsp;',2)."/".str_repeat('&nbsp;',2).$data2['nmdivisi']. "<br/>";
if($data2['pt']=="AAL0"){
  echo "<br/>".str_repeat('&nbsp;',3)."<b>DIREKTORAT".str_repeat('&nbsp;',11).":<font style='text-align: left; background-color:Yellow;'>".$data2['nmdirektorat']."</font> </b>";
}
else{
  echo "<br/>".str_repeat('&nbsp;',3)."<b>NAMA PT.SITE".str_repeat('&nbsp;',11).":</b>".$data2['nmpt'].str_repeat('&nbsp;',2)."-(".$data2['pt'].")".str_repeat('&nbsp;',2).str_repeat('&nbsp;',80)."<b>AREA :</b>".$data2['area'];
}

echo "<br/>";
echo "</p>";
?>
<hr />
<?php
echo "<p style='text-align: left; background-color:Gainsboro;'>".str_repeat('&nbsp;',2)." <b>VERIFIKASI: </b> </p>";
echo "<p  style='margin:0; font-size:11px; font-family:Arial;'>";
echo str_repeat('&nbsp;',2)."<u><b>Spek.Perangkat saat ini :</b></u>". "<br/>";
echo str_repeat('&nbsp;',2).$data2['rekomendasi_ver'] . "<br/>". "<br/>";
echo str_repeat('&nbsp;',2)."<u><b>Issue :</b></u>". "<br/>". str_repeat('&nbsp;',2).$data2['issue'] ."<br/>". "<br/>";
echo str_repeat('&nbsp;',2)."<u><b>Penjelasan :</b></u>". "<br/>". str_repeat('&nbsp;',2).$data2['penjelasan'] ."<br/>". "<br/>";
echo str_repeat('&nbsp;',2)."<u><b>Rekomendasi :</b></u>". "<br/>". str_repeat('&nbsp;',2).$data2['rekomendasi_ver'] . "<br/>". "<br/>";
echo str_repeat('&nbsp;',2)."<u><b>Lampiran :</b></u>". "<br/>". str_repeat('&nbsp;',2).$data2['lampiran']. "<br/>" ;
echo "<br/>";
echo "</p>";
echo "<table border=0 width='1276px' style='margin:0; font-size:11px; font-family:Arial;' >";
echo "<tr  width='40%'>
          <td> ".str_repeat('&nbsp; ',10)." </td>
          <td style='text-align: center;'> TTD VERIFIKATOR, </td>
     </tr>";
echo "<tr  width='40%'>
          <td>".str_repeat('&nbsp; ',10)." </td>
          <td>".str_repeat('&nbsp; ',10)." </td>
     </tr>";
echo "<tr  width='40%'>
          <td>".str_repeat('&nbsp; ',10)." </td>
          <td>".str_repeat('&nbsp; ',10)." </td>
     </tr>";

echo "<tr  width='40%'>
          <td>".str_repeat('&nbsp; ',10)." </td>
          <td style='text-align: center;'><b>".$data2['nmapprov1']."</b> </td>
     </tr>";
echo "</table>";
echo "<br/>";

?>
<hr />
<?php

echo "<p style='text-align: left; background-color:Gainsboro;'>".str_repeat('&nbsp;',2)." <b>REKOMENDASI: </b> </p>";
echo "<p  style='margin:0; font-size:11px; font-family:Arial;'>";
echo "<br/>";
echo "<table border=0 width='800px' style='margin:0; font-size:11px; font-family:Arial;' >";
echo "<tr>
          <td width='600px'> <font style='text-align: left; background-color:Yellow;'>".$data2['nmunit'].
		  " </td> <td width='200px' style='text-align: left;'> <font style='text-align: left; background-color:Yellow;'>QTY:".str_repeat('&nbsp;',2).$data2['quantity']." </font> </td>
     </tr>";
echo "<tr>
          <td>".$data2['nmspek1']."<br/>".$data2['nmspek2']." </td>
     </tr>";
echo "</table>";
echo "<br/>";
echo str_repeat('&nbsp;',2)."<b> Harga perkiraan: </b> US $".number_format($data2['harga2'])."/Rp.".number_format($data2['harga']);
echo "<br/>";
echo "<br/>";
echo str_repeat('&nbsp;',2)."<b> Notes: </b>";
echo str_repeat('&nbsp;',2).$data2['notes'];
echo "</p>";
echo "<br/>";
?>
<hr />
<?php
//. . .  / . . . / . . . . . .
echo "<p  style='margin:0; font-size:11px; font-family:Arial;'>";
echo str_repeat('&nbsp;',2)."Jakarta, ".tgl_indo($data2['tgl_spc']);
echo "<br/>";
echo "<br/>";
echo "<table border=0  height='10%'  cellspacing='0' cellpadding='0' >";
echo "<tr> <td style='width:50%'> Dibuat Oleh,   <td/>  <td style='width:50%; text-align: center; '> Mengetahui  <td/>    </tr>";
echo "<tr > <td> ".str_repeat('&nbsp;',20)." <td/>  <td>   <td/>    </tr>";
echo "<tr > <td> ".str_repeat('&nbsp;',20)."  <td/>  <td>   <td/>    </tr>";
echo "<tr width='50%'>  <td>".str_repeat('&nbsp; ',10)." </td>  <td>".str_repeat('&nbsp; ',10)." </td>   </tr>";
echo "<tr width='50%'>  <td>".str_repeat('&nbsp; ',10)." </td>  <td>".str_repeat('&nbsp; ',10)." </td>   </tr>";
echo "<tr> 
        <td> <u>( <b>".trim($data2['nmapprov1'])."</b> )</u>".str_repeat('&nbsp;',80)." <td/> 
		<td style='text-align: center;'> <u>( <b> ".str_repeat('&nbsp;',2).trim($data2['nmapprov3']).str_repeat('&nbsp;',1).str_repeat('&nbsp;',2)." <b> )</u> <td/> 
      </tr>";
echo "<tr>
          <td> ".$data2['nmjabatan1'].str_repeat('&nbsp;',80)."  </td>
          <td style='text-align: center;'> ".str_repeat('&nbsp;',2).$data2['nmjabatan3'].str_repeat('&nbsp;',2)."  </td>
     </tr>";
echo "</table>";
echo "</p>";
?>
</td></tr>
</table>

<!--CONTOH Code END-->
</div>
</body>

 
<?php
$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->mirrorMargins = true;
$mpdf=new mPDF('c','A4'); 
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');
exit;
?>
</html>
