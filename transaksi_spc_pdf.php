<?php
    session_start();
    include "koneksi.php";
	include ("ref_fun.php");
    require('fpdf/fpdf.php');
    
	//echo '  Pencarian: '.$idbr;
     
    //$query ="select * from master_perangkat";
    $query = "select * from tran_spc_induk where status='Y' and id_spc like '%$_GET[idspc]%' ";
    $result = mysql_query($query);
	$baris=1; //menambahkan variabel baris
    //Variabel untuk iterasi
    $i = 0;
	$tinggi=0.5;
    //Mengambil nilai dari query database
    while($data=mysql_fetch_row($result))
    {
		$cell[$i][0] = $data[0];
        $cell[$i][1] = $data[1];
		$cell[$i][2] = $data[2];
        $cell[$i][3] = $data[3];
        $cell[$i][4] = $data[5];
        $cell[$i][5] = $data[6];
		$cell[$i][6] = $data[6];
		$cell[$i][7] = $data[7];
		$cell[$i][8] = $data[8];
		$cell[$i][9] = $data[9];
		$cell[$i][10] = $data[10];
		$cell[$i][11] = $data[12];
		$cell[$i][12] = $data[13];
		$cell[$i][13] = $data[14];
		$cell[$i][14] = $data[15];
		$cell[$i][15] = $data[16];
		$cell[$i][16] = $data[17];
		$cell[$i][17] = $data[18];
		$cell[$i][18] = $data[8];
        $i++;
    }
    //memulai pengaturan output PDF
    class PDF extends FPDF
    {
        //untuk pengaturan header halaman
        function Header()
        {
            //Pengaturan Font Header
            $this->SetFont('Times','B',14); //jenis font : Times New Romans, Bold, ukuran 14
            //untuk warna background Header
            $this->SetFillColor(255,255,255);
            //untuk warna text
            $this->SetTextColor(0,0,0);
            //Menampilkan tulisan di halaman
            $this->Cell(20,1,'SLIP PERMINTAAN COMPUTER (SPC)','B',0,'C',0); 
			//TBLR (untuk garis)=> B = Bottom, L = Left, R = Right, untuk garis, C = center
			$this->SetFont('Arial','B',7.5); //jenis font : Times New Romans, Bold, ukuran 14
			$this->Ln(1);
			$this->image('images/logo_aal.png',0,1,1.5,1);
			//$this->Image('logo.png',10,6,30);
			
           //Ln() = untuk pindah baris
           //$pdf->SetFont('Times','B',12);
//		$pdf->MultiCell(2.1,0.5,$cell[$j][0],'LBTR',0,'C');
//		 $this->Ln();
			
        }
    function Footer()
    {
    // Posisi 15 cm dari bawah
    $this->SetY(-6);
    // Arial italic 8
    $this->SetFont('Arial','',8);
    // Page number
    $this->Cell(0,10,'Hal. '.$this->PageNo().'/{nb}',0,0,'C');
    }

}
// Page footer
	
    //pengaturan ukuran kertas P = Portrait L=LANSCAPE
    $pdf = new PDF('P','cm','A4');
	$pdf->SetMargins(0.0,1,0.0);
    $pdf->Open();
          //Alias total halaman dengan default {nb} (berhubungan dengan PageNo())
     $pdf->AliasNbPages();
     $pdf->AddPage();
   
    $pdf->SetFont('Times',"",8);
	//$pdf->SetHeight(0.1);
	

  for($j=0;$j<$i;$j++)
    {
        //menampilkan data dari hasil query database
        //$pdf->Cell(3,1,$j+1,'LBTR',0,'C');
//		$pdf->MultiCell(2.1,0.5,$cell[$j][0],'LBTR',0,'C');
        $pdf->Cell(12,0.5,'NOMOR SPC:'.$cell[$j][0],'LTB',0,'L');
		$pdf->Cell(8,0.5,'TANGGAL :'.$cell[$j][1],'TBR',0,'R');
        $pdf->Ln(1);
		$pdf->Cell(20,0.5,'PT :'.$cell[$j][2],'LTR',0,'L');
		$pdf->Ln(0.5);
		$pdf->Cell(3,0.5,'DIVISI :'.$cell[$j][14],'LB',0,'L');
		$pdf->Cell(17,0.5,'DEPARTEMENT :'.$cell[$j][13],'BR',0,'R');
		$pdf->Ln(0.5);
		$pdf->Cell(1,0.5,'NO','LTBR',0,'C');
		$pdf->Cell(5,0.5,'NAMA BARANG','LTBR',0,'C');
		$pdf->Cell(8,0.5,'SPESIFIKASI','LTBR',0,'C');
		$pdf->Cell(1,0.5,'QTY','LTBR',0,'C');
		$pdf->Cell(5,0.5,'TTD PEMOHON','LTBR',0,'C');
		$pdf->Ln(0.5);
		$pdf->Cell(1,0.5,'','LTBR',0,'C');
		$pdf->Cell(5,0.5,'','LTBR',0,'C');
		$pdf->Cell(8,0.5,'','LTBR',0,'C');
		$pdf->Cell(1,0.5,'','LTBR',0,'C');
		$pdf->Cell(5,0.5,'','LTBR',0,'C');
		$pdf->Ln(0.5);
		$pdf->Cell(1,0.5,'','LTBR',0,'C');
		$pdf->Cell(5,0.5,'','LTBR',0,'C');
		$pdf->Cell(8,0.5,'','LTBR',0,'C');
		$pdf->Cell(1,0.5,'','LTBR',0,'C');
		$pdf->Cell(5,0.5,'','LTBR',0,'C');
		$pdf->Ln(0.5);
		$pdf->Cell(1,0.5,'','LTBR',0,'C');
		$pdf->Cell(5,0.5,'','LTBR',0,'C');
		$pdf->Cell(8,0.5,'','LTBR',0,'C');
		$pdf->Cell(1,0.5,'','LTBR',0,'C');
		$pdf->Cell(5,0.5,'','LTBR',0,'C');
		
		$pdf->Ln(1);
		$pdf->Cell(20,0.5,'KETERANGAN ','TLR',0,'U');
		$pdf->Ln(0.5);
		$pdf->Cell(8,0.5,'1. Nama user/ PIC Komputer :'.$cell[$j][11],'L',0,'L');
		$pdf->Cell(12,0.5,' NPK :'.$cell[$j][6],'R',0,'C');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'2. Jumlah Personil di Divisi :','LR',0,'L');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'3. Jumlah Komputer  :'.$cell[$j][7],'LR',0,'L');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'4. Printer tersedia di Divisi  :'.$cell[$j][8],'LR',0,'L');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'5. Spesifikasi barang lama  :'.$cell[$j][9],'LRB',0,'L');
		
		$pdf->Ln(1);
		$pdf->Cell(20,0.5,'URAIAN ALASAN PERMINTAAN (USER) DENGAN MELAMPIRKAN JOB DESC.','TLR',0,'U');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'1.'.$cell[$j][4],'LR',0,'L');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'','LR',0,'C');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'','LR',0,'C');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'','LBR',0,'C');
		
		$pdf->Ln(1);
		$pdf->Cell(20,0.5,'REKOMENDASI PENGADAAN OLEH DIVISI GA :','TLR',0,'U');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'','LR',0,'L');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'','LR',0,'C');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'','LR',0,'C');
		$pdf->Ln(0.5);
		$pdf->Cell(20,0.5,'','LBR',0,'C');
        $pdf->Ln();
	}

    $pdf->Output();

	
?>