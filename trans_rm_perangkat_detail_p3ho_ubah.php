<?php
include "menu2.php";
include ("koneksi.php");
include ("ref_fun.php");
global $iddrm,$iddtgl  ;
session_start();
$idrm=$_SESSION['idrm']  ;
$idtgl=$_SESSION['idtgl']  ;
$idsn=$_GET['id']  ;
$unit=$_SESSION['unit'];

$query = "select a.*,
          get_nmrusak('$unit',a.ho_cek_rusak1) horusak1, 
          get_nmrusak('$unit',a.ho_cek_rusak2) horusak2, 
          get_nmrusak('$unit',a.ho_cek_rusak3) horusak3,
          get_nmrusak('$unit',a.p3_cek_rusak1) p3rusak1, 
          get_nmrusak('$unit',a.p3_cek_rusak2) p3rusak2, 
          get_nmrusak('$unit',a.p3_cek_rusak3) p3rusak3
          from tran_rm_asset_det a  where sn_asset like '%".$idsn."%' and reg_rm like '%".$idrm."%' and tgl_rm like '%".$idtgl."%'  ";
//echo "INI :".$query;

$result = mysql_query($query);
$data = mysql_fetch_array($result);
	
	$reg_rm=$data['reg_rm'];
    $tgl_rm=$data['tgl_rm'];
    $sn_asset=$data['sn_asset'];
    $ho_cek_rusak1=$data['ho_cek_rusak1']."-".$data['horusak1'];
    $ho_cek_rusak2=$data['ho_cek_rusak2']."-".$data['horusak2'];
    $ho_cek_rusak3=$data['ho_cek_rusak3']."-".$data['horusak3'];
    $p3_cek_rusak1=$data['p3_cek_rusak1']."-".$data['p3rusak1'];
    $p3_cek_rusak2=$data['p3_cek_rusak2']."-".$data['p3rusak2'];
    $p3_cek_rusak3=$data['p3_cek_rusak3']."-".$data['p3rusak3'];
	
	$p3ho_tgl_terima=$data['p3ho_tgl_terima'];
	$p3ho_pic_terima=$data['p3ho_pic_terima'];
	$p3ho_tgl_cek=$data['p3ho_tgl_cek'];
	$p3ho_cek_perbaikan=$data['p3ho_cek_perbaikan'];
	$p3ho_status=$data['p3ho_status'];

?>

<html>
<head>
<title> Update RM Perangkat Detail/SN Lokasi HO dari Venedor </title>
</head>
<body OnLoad="document.formrm.p3ho_cek_rusak.focus();">
<link type="text/css" href="asset/themes/base/ui.all.css" rel="stylesheet" />   
    <script type="text/javascript" src="asset/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="asset/ui/ui.core.js"></script>
    <script type="text/javascript" src="asset/ui/ui.datepicker.js"></script>   
    <script type="text/javascript" src="asset/ui/i18n/ui.datepicker-id.js"></script>
    <script type="text/javascript" src="asset/ui/effects.core.js"></script>
    <script type="text/javascript" src="asset/ui/effects.drop.js"></script>

    <script type="text/javascript"> 
      $(document).ready(function(){
        $("#p3ho_tgl_terima").datepicker({
          showAnim    : "drop",
          showOptions : { direction: "up" }
        });
      });
	  </script>
    <script type="text/javascript"> 
      $(document).ready(function(){
        $("#p3ho_tgl_cek").datepicker({
          showAnim    : "drop",
          showOptions : { direction: "up" }
        });
      });
	  </script>
	  
<?php  echo "<p style='font-size:16px;' > <b>Update RM Perangkat Detail/SN Lokasi HO dari Vendor </b> ".
            "<b>:</b>".$idrm." <b> ~</b>  :".tgl_indo2($idtgl)."<b> NO SN:</b> ".$sn_asset ."</p>"; ?>
<hr />

<form method="post" name="formrm" action="trans_rm_perangkat_detail_p3ho_ubah_query.php" >
<table width='95%' style="font-size:12px; font-family:Arial;" rules='none' border="1" cellspacing="1" cellpadding="1" align="center"  >
    <tr>
          <td bgcolor="#C0C0C0">NO REG RM</td>
          <td bgcolor="#F0F8FF">: <?php echo $reg_rm; ?></td>
          <input type="hidden" name="reg_rm" value="<?php echo $reg_rm; ?>" >

          <td bgcolor="#C0C0C0">TGL RM</td>
          <td bgcolor="#F0F8FF">: <?php echo $tgl_rm; ?></td>
          <input type="hidden" name="tgl_rm" value="<?php echo $tgl_rm; ?>" >
    </tr>
    <tr>
          <td bgcolor="#C0C0C0">SN ASSET</td>
          <td bgcolor="#F0F8FF">: <?php echo $sn_asset; ?></td>
          <input type="hidden" name="sn_asset" value="<?php echo $sn_asset; ?>" >
    </tr>
	<tr>
    <td bgcolor="#C0C0C0">HO CHEK KRUSAKAN 1.</td>
    <td bgcolor="#F0F8FF">: <?php echo $ho_cek_rusak1; ?></td>
    <td bgcolor="#C0C0C0">VENDOR KRUSAKAN 1.</td>
    <td bgcolor="#F0F8FF">: <?php echo $p3_cek_rusak1; ?></td>
	</tr>	
	<tr>
    <td bgcolor="#C0C0C0">HO CHEK KRUSAKAN 2.</td>
    <td bgcolor="#F0F8FF">: <?php echo $ho_cek_rusak2; ?></td>
    <td bgcolor="#C0C0C0">VENDOR KRUSAKAN 2.</td>
    <td bgcolor="#F0F8FF">: <?php echo $p3_cek_rusak2; ?></td>
	</tr>	
	<tr>
    <td bgcolor="#C0C0C0">HO CHEK KRUSAKAN 3.</td>
    <td bgcolor="#F0F8FF">: <?php echo $ho_cek_rusak3; ?></td>
    <td bgcolor="#C0C0C0">VENDOR KRUSAKAN 3.</td>
    <td bgcolor="#F0F8FF">: <?php echo $p3_cek_rusak3; ?></td>
	</tr>	

    <tr>
      <td bgcolor="#C0C0C0">TGL TERIMA </td>
      <td>: <input type="date" id="p3ho_tgl_terima" name="p3ho_tgl_terima" maxlength="10" size="10" value="<?php echo tgl_normal($p3ho_tgl_terima); ?>" required>

    <td bgcolor="#C0C0C0">PIC TERIMA </td>
	<td>:<select name="p3ho_pic_terima">
	<option value=0 selected>-Pilih-</option>
    <?php
    $query=("select pic,nama_pic from master_pic where jabatan like '%Staf' and divisi='IT' and status='Y' ");
    $query_hasil=mysql_query($query);
    while($r=mysql_fetch_array($query_hasil)) {
      if ($data['p3ho_pic_terima'] == $r[pic])  {
         echo "<option selected='selected' value=$r[pic]>$r[pic].$r[nama_pic]</option>";
      } else {
         echo "<option value=$r[pic]>$r[pic].$r[nama_pic]</option>";
      } 	 }
    ?>
	</select>
	</td>
	</tr>	
    <tr>
      <td bgcolor="#C0C0C0">TGL PNGECEKAN </td>
      <td>: <input type="date" id="p3ho_tgl_cek" name="p3ho_tgl_cek" maxlength="10" size="10" value="<?php echo tgl_normal($p3ho_tgl_cek); ?>" required>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">CEK PERBAIKAN</td>
        <td>:<?php // echo $ket; ?>
	    <textarea style='text-align: left;' name="p3ho_cek_perbaikan" maxlength="150" cols="50" rows="1" value="<?php echo $p3ho_cek_perbaikan; ?>" required> <?php echo $p3ho_cek_perbaikan; ?> </textarea ></td>
    </tr>
  <tr>
    <td bgcolor="#C0C0C0">RM SELESAI </td>
    <td>:<select name="p3ho_status">
   	<option <?php if ($p3ho_status=='N'){echo "selected";}?> value='N'>N</option>
	<option <?php if ($p3ho_status=='Y'){echo "selected";}?> value='Y'>Y</option>
  </tr>

    <td>&nbsp;</td>
    <td><input type="submit" name="submit" value="SIMPAN">
    <input type="button" value="CLOSE" onclick="javascript:self.close()"></td>
  </tr>
</table>

</form>
</body>
</html>

