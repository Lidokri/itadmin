<?php
#koneksi
$conn = mysqli_connect("localhost", "root", "", "itadmin");
#ambil data master perangkat
$query = "SELECT sn_asset,no_asset FROM master_asset_utama 
          where sn_asset<10
          ORDER BY sn_asset";
//echo "INI :".$query;
$sql = mysqli_query($conn, $query);
$arrperangkat = array();
while ($row = mysqli_fetch_assoc($sql)) {
	$arrperangkat [ $row['no_asset'] ] = $row['sn_asset'];
	}
?>
<html>
	<head>
		<title>JQuery Multiple Select</title>
		<script src="asset/jquery.min.js"></script>
		<script src="asset/jquery.multiple.select.js"></script>
		<link rel="stylesheet" href="asset/multiple-select.css"/>
		<script>
			$(document).ready(function(){
				$('#pilasset').multipleSelect({
					placeholder: "Pilih SN",
					filter:true
				});
			});
		</script>

<style>		
h2 {
    display: block;
    font-size: 1.5em;
    margin-top: 0.13em;
    margin-bottom: 0.13em;
    margin-left: 0;
    margin-right: 0;
    font-weight: bold;
}		
h3 {
    display: block;
    font-size: 1.17 em;
    margin-top: 0.10em;
    margin-bottom: 0.10em;
    margin-left: 0;
    margin-right: 0;
    font-weight: bold;
}		
</style>
	</head>
	<body>
		<h2>JQuery Multiple Select Demo</h2>
		<h3>Demo #3 : Combobox dengan Placeholder dan Filter</h3>

		<form action="" method="post">
		<select id="pilasset" name="pilasset[]" multiple="multiple" style="width:300px">
			<?php
			foreach($arrperangkat as $no_asset=>$sn_asset) {
				echo "<option value='$sn_asset'>$sn_asset</option>";
			}
			?>
		</select>
		<input type="submit" name="Pilih" value="Pilih"/>
		</form>
		<?php
		if(isset($_POST['Pilih'])) {
			echo "Propinsi yang Anda pilih: <br/>";

			$isi=implode(", ",$_POST['pilasset']);
			echo "Ini :".$isi;
		}
		?>
		
		
	</body>
</html>
