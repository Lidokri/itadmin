<?php
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	session_start();
	if(isset($_SESSION['username'])==""):
	   echo "<meta http-equiv='refresh' content='1 url=logout.php'>";
	else:
	   $useron=$_SESSION['username'];
	endif;

	include "../menu3.php";
	include ("../koneksi.php");
	include ("../ref_fun.php");

	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
	global $iddrm,$iddtgl  ;
	session_start();
	$useron=$_SESSION['username'];

	$tgl=$_SESSION['tgl'];
	$unit=$_SESSION['unit'];
	$tgl_rm=$_SESSION['tgl_rm'];
	$pilkol=$_SESSION['pilkol'];
	$pilisi=$_SESSION['pilisi'];

	#ambil data master perangkat
	$query = "SELECT sn_asset,no_asset FROM master_asset_utama 
	                 Where status='Y' and unit like '%".$unit."%' 
	                 and sn_asset not in (select distinct(sn_asset) from tran_rm_asset_det where 
					                             p3ho_status='N')
					 ORDER BY sn_asset ";
	//echo "INI :".$query;

	$sql = mysqli_query($conn, $query);
	$arrperangkat = array();
	while ($row = mysqli_fetch_assoc($sql)) {
		$arrperangkat [ $row['no_asset'] ] = $row['sn_asset'];
		}
?>
<html lang="en">
<head>
	<title>REPAIR MAINTENANCE PERANGKAT</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
    <link rel="shortcut icon" href="images/logo_aal.png">
	</style>
    <link rel="stylesheet" href="../asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../asset/css/custom.css">
    <link type="text/css" href="../asset/themes/base/ui.all.css" rel="stylesheet" />

    <script src="../asset/bootstrap/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../asset/bootstrap/js/jquery-ui-1.10.0.custom.min.js" type="text/javascript"></script>
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../asset/ui/ui.core.js"></script>

    <script type="text/javascript" src="../asset/ui/ui.datepicker.js"></script>   
    <script type="text/javascript" src="../asset/ui/i18n/ui.datepicker-id.js"></script>
    <script type="text/javascript" src="../asset/ui/effects.core.js"></script>
    <script type="text/javascript" src="../asset/ui/effects.drop.js"></script>
    <style type="text/css">
	h3 {
	    display: block;
	    font-size: 0.97 em;
	    margin-top: 0.06em;
	    margin-bottom: 0.06em;
	    margin-left: 0;
	    margin-right: 0;
	    font-weight: bold;
	}	
	</style>
	<script type="text/javascript"> 
      $(document).ready(function(){
        $("#site_tgl_kirim").datepicker({
          showAnim    : "drop",
          showOptions : { direction: "up" }
        });
      });
	  </script>
	<style type="text/css">	
	h4 {
      display: block;
      font-size: 16px;
      margin-top: 1em;
      margin-bottom: 0.5em;
      margin-left: 1.0em;
      margin-right: 0em;
      }	

	</style>
	<script type="text/javascript">
		function ceksn_asset()
		{
	        if(document.getElementById('reg_rm').value == '')
			{  
	        	alert('sn asset tidak boleh kosong');  
	           	document.getElementById('reg_rm').focus();  
	           	return false;  
	        }  
		}
	</script>

	<script type="text/javascript" src="../asset/dist/js/standalone/selectize.min.js"></script>
    <link rel="stylesheet" href="../asset/dist/css/selectize.bootstrap3.css">  
</head>
<body>
	<section class="header-rm-perangkat">
		<div class="col-md-12 top-margin text-center div-bottom-margin">
			<h3>REPAIR MAINTENANCE PERANGKAT</h3>
			<b>Unit:</b> DC0001 - <b>Tgl:</b> 01/07/2016 - <b>Hasil Kolom:</b> reg_rm - <b>Isi:</b>
		</div>
	</section>
	<section class="detail-panel">
		<div class="col-md-12">
			<div class="panel panel-default">
			  <div class="panel-heading"><b>Total Perangkat</b></div>
			  	<div class="panel-body">
			  		<form class="form-horizontal">
					  <div class="form-group" style="margin-bottom:0">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">Pilih No SN <?php echo "<b>". $unit." </b>"; ?></label>
					    <div class="col-sm-4">
					      <select id="select-sn" name="state[]" multiple class="form-control"  style="width:400px" placeholder="Pilih Total Perangkat">
							<?php
							foreach($arrperangkat as $no_asset=>$sn_asset) {
								echo "<option value='$sn_asset'>$sn_asset</option>";
							}
							?>
						</select>
					    </div>
					    <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#detail" aria-expanded="false" aria-controls="detail">Simpan</button>
					  </div>
					 </form>
					<script>
						$('#select-sn').selectize({
						});
					</script>
				</div>
			  </div>
			</div>
	</section>
	<section class="detail-panel">
		<div class="col-md-12">
			<div class="panel panel-default">
			  <div class="panel-heading"><b>Detail</b></div>
			  	<div class="collapse panel-body" id="detail">
			  		<form class="form-horizontal">
					  <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">Tanggal RM</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="Tanggal RM" disabled>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">Det SN</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="Det SN" disabled>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">Jumlah Det SN</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="Jumlah Det SN" disabled>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">No Reg RM</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="No Reg RM" disabled>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">PT</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="PT">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">Departemen</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="Departemen">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">PIC User</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="PIC User">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">Ref TT</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="inputEmail3" placeholder="Ref TT">
					    </div>
					  </div>

					  <div class="form-group">
					    <div class="col-sm-10">
					      <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#lokasi" aria-expanded="false" aria-controls="lokasi">Simpan</button>
					    </div>
					  </div>
					</form>
				</div>
			  </div>
			</div>
	</section>
	

	<section class="detail-panel">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">	<?php  echo "<p style='font-size:16px;' > <b>Update RM Perangkat Detail/SN Lokasi SITE </b> "."<b>:</b>".$unit."-".$idrm." <b> ~</b>  :".tgl_indo2($idtgl)."<b> NO SN:</b> ".$sn_asset ."</p>";?>
	        	</div>
				<div class="collapse panel-body" id="lokasi">
					<form class="form-horizontal" method="post" name="formrm"  >
						<!-- action="trans_rm_perangkat_detail_site_ubah_query.php" -->
						<div class="form-group">
	                      	<label for="inputEmail3" class="col-sm-2 control-label"  style="text-align:left">NO REG RM</label>
	                      	<div class="col-sm-4">
	                        	<input type="text" class="form-control" disabled>
	                      	</div>
	                    </div>
	                   
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">TGL RM</label>
	                      	<div class="col-sm-4">
	                        	<input type="text" class="form-control" disabled>
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">SN ASSET</label>
	                      	<div class="col-sm-4">
	                        	<input type="text" class="form-control" disabled>
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">CHEK KERUSAKAN 1</label>
	                      	<div class="col-sm-4">
	                        	<select class="form-control">
			                        <option>option 1</option>
			                        <option>option 2</option>
			                        <option>option 3</option>
			                        <option>option 4</option>
			                        <option>option 5</option>
			                    </select>
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">CHEK KERUSAKAN 2</label>
	                      	<div class="col-sm-4">
	                        	<select class="form-control">
			                        <option>option 1</option>
			                        <option>option 2</option>
			                        <option>option 3</option>
			                        <option>option 4</option>
			                        <option>option 5</option>
			                    </select>
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">CHEK KERUSAKAN 3</label>
	                      	<div class="col-sm-4">
	                        	<select class="form-control">
			                        <option>option 1</option>
			                        <option>option 2</option>
			                        <option>option 3</option>
			                        <option>option 4</option>
			                        <option>option 5</option>
			                    </select>
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">PERBAIKAN</label>
	                      	<div class="col-sm-4">
	                        	​<textarea class="form-control" rows="5" id="comment"></textarea>
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">PERLU PART</label>
	                      	<div class="col-sm-4">
	                        	<select class="form-control">
			                        <option>option 1</option>
			                        <option>option 2</option>
			                        <option>option 3</option>
			                        <option>option 4</option>
			                        <option>option 5</option>
			                    </select>
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">RM SELESAI</label>
	                      	<div class="col-sm-4">
	                        	<select class="form-control">
			                        <option>option 1</option>
			                        <option>option 2</option>
			                        <option>option 3</option>
			                        <option>option 4</option>
			                        <option>option 5</option>
			                    </select>
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">TGL KIRIM</label>
	                      	<div class="col-sm-4">
	                        	<input type="date" class="form-control">
	                      	</div>
	                    </div>
	                    
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">PIC SITE</label>
	                      	<div class="col-sm-4">
	                        	<select class="form-control">
			                        <option>option 1</option>
			                        <option>option 2</option>
			                        <option>option 3</option>
			                        <option>option 4</option>
			                        <option>option 5</option>
			                    </select>
	                      	</div>
	                    </div>   
	                    <div class="form-group">
	                      	<label class="col-sm-2 control-label"  style="text-align:left">KIRIM VIA-NO.RESI </label>
	                      	<div class="col-sm-4">
	                        	<input type="text" class="form-control">
	                      	</div>
	                    </div>
						<div class="form-group">
					    	<div class="col-sm-10">
					      		<button type="button" class="btn btn-success" data-toggle="collapse" data-target="#site" aria-expanded="false" aria-controls="site">Simpan</button>
					    	</div>
					 	 </div>
					</form>
				</div>
			</div>
		</div>		
	</section>

	<section class="detail-panel">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">	<?php  echo "<p style='font-size:16px;' > <b>Update RM Perangkat Detail/SN Lokasi SITE </b> ".
        "<b>:</b>".$unit."-".$idrm." <b> ~</b>  :".tgl_indo2($idtgl)."<b> NO SN:</b> ".$sn_asset ."</p>"; ?>
	        	</div>
				<form class="form-horizontal" method="post" name="formrm"  >
					<div class="collapse panel-body" id="site">
						<div class="row">
							<div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">NO REG RM</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">TGL RM</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
						</div>
						<div class="row">
							<div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">SN ASSET</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
						</div>
 						<div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">TGL TERIMA</label>
		                      	<div class="col-sm-5">
		                        	<input type="date" class="form-control">
		                      	</div>
		                    </div>
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PIC TERIMA</label>
		                      	<div class="col-sm-5">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
						</div>
						<div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">TGL PENGECEKAN</label>
		                      	<div class="col-sm-5">
		                        	<input type="date" class="form-control">
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">SITE CHEK KERUSAKAN 1</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                     <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">HO CHEK KERUSAKAN 1</label>
		                      	<div class="col-sm-5">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">    
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">SITE CHEK KERUSAKAN 2</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                     <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">HO CHEK KERUSAKAN 2</label>
		                      	<div class="col-sm-5">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">SITE CHEK KERUSAKAN 3</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                    <div class="form-group col-sm-6">
		                      	<label class="col-sm-5 control-label"  style="text-align:left">HO CHEK KERUSAKAN 3</label>
		                      	<div class="col-sm-5">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PERBAIKAN</label>
		                      	<div class="col-sm-7">
		                        	<textarea class="form-control" rows="5" id="comment"></textarea>
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PERLU PART</label>
		                      	<div class="col-sm-7">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
		                </div>		                <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">RM SELESAI</label>
		                      	<div class="col-sm-7">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">TGL KIRIM VENDOR</label>
		                      	<div class="col-sm-5">
		                        	<input type="date" class="form-control">
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PIC KIRIM</label>
		                      	<div class="col-sm-3">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">KIRIM VIA-NO.RESI </label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control">
		                      	</div>
		                    </div>		
						</div>

		                    <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#vendor" aria-expanded="false" aria-controls="vendor">Simpan</button>
	
					</div>
				</form>
			</div>
		</div>		
	</section>
	<section class="content">
		<div class="col-md-12" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<!-- <hr style="height:10px; background-color:grey"> -->
					<?php  echo "<p style='font-size:16px;' > <b>Update RM Perangkat Detail/SN Lokasi VENDOR </b> ".
		            "<b>:</b>".$idrm." <b> ~</b>  :".tgl_indo2($idtgl)."<b> NO SN:</b> ".$sn_asset ."</p>"; ?>
				</div>
				<form method="post" name="formrm" >
					<div class="collapse panel-body" id="vendor">
						<div class="row">
							<div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">NO REG RM</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">TGL RM</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                </div>
		                <div class="row">
				                    <div class="form-group col-sm-6">
				                      	<label  class="col-sm-5 control-label"  style="text-align:left">SN ASSET</label>
				                      	<div class="col-sm-5">
				                        	<input type="text" class="form-control" disabled>
				                      	</div>
				                    </div>
				                    <div class="form-group col-sm-6">
				                      	<label  class="col-sm-5 control-label"  style="text-align:left">TGL TERIMA</label>
				                      	<div class="col-sm-5">
				                        	<input type="date" class="form-control">
				                      	</div>
				                    </div>
				        </div>
				        <div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">HO CHEK KERUSAKAN 1</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PIHAK 3 CHEK KERUSAKAN 1</label>
		                      	<div class="col-sm-5">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
			            </div>  
						<div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">HO CHEK KERUSAKAN 2</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PIHAK 3 CHEK KERUSAKAN 2</label>
		                      	<div class="col-sm-5">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
				        </div>
						<div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">HO CHEK KERUSAKAN 3</label>
		                      	<div class="col-sm-5">
		                        	<input type="text" class="form-control" disabled>
		                      	</div>
		                    </div>
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PIHAK 3 CHEK KERUSAKAN 3</label>
		                      	<div class="col-sm-5">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
				        </div>
						<div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PERSETUJUAN</label>
		                      	<div class="col-sm-3">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
		                    
		                    <div class="form-group col-sm-6"">
		                      	<label class="col-sm-5 control-label">PERLU PART</label>
		                      	<div class="col-sm-3">
		                        	<select class="form-control">
				                        <option>option 1</option>
				                        <option>option 2</option>
				                        <option>option 3</option>
				                        <option>option 4</option>
				                        <option>option 5</option>
				                    </select>
		                      	</div>
		                    </div>
						</div>
						<div class="row">
		                    <div class="form-group col-sm-6">
		                      	<label class="col-sm-5 control-label">TGL KIRIM</label>
		                      	<div class="col-sm-5">
		                        	<input type="date" class="form-control">
		                      	</div>
		                    </div>
	                    </div>
	                    <div class="row">
	                    	<div class="form-group col-sm-6">
		                      	<label  class="col-sm-5 control-label"  style="text-align:left">PERBAIKAN</label>
		                      	<div class="col-sm-7">
		                        	<textarea class="form-control" rows="5" id="comment"></textarea>
		                      	</div>
		                    </div>
		                    <div class="form-group col-sm-6">
		                      	<label class="col-sm-5 control-label">KIRIM VIA-NO.RESI </label>
		                      	<div class="col-sm-7">
		                        	<input type="text" class="form-control">
		                      	</div>
		                    </div>		
						</div>
	                    <button type="submit" class="btn btn-success">Simpan</button>
					</div>
				</form>
			</div>
		</div>		
	</section> 		
</body>
</html>