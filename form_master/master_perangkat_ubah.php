<?php
//include "../menu2.php";
include ("../koneksi.php");
include ("../ref_fun.php");

$query = "select a.*, b.nama_grup, a.unit, b.satuan,
          (select nama_tipe from master_tipe c  where id_tipe=a.id_tipe) nama_tipe
FROM master_perangkat_part a, master_grup b 
where a.id_grup = b.id_grup and a.unit = '$_GET[id]'";

$result = mysql_query($query);
$data = mysql_fetch_array($result);

	$id_unit=$data['id_unit'];
    $id_tipe=$data['id_tipe']; 
	$id_grup=$data['id_grup'];
	$nama_grup=$data['nama_grup'];
	$periode=$data['periode'];
	$unit=$data['unit'];
    $nama_unit=$data['nama_unit'];
	$nama_tipe=$data['nama_tipe'];
	$spek1=$data['spek1'];
    $spek2=$data['spek2'];
    $spek3=$data['spek3'];
	$satuan=$data['satuan'];
    $jumlah_stok=$data['jumlah_stok'];
    $harga=$data['harga'];
    $harga2=$data['harga2'];
    $garansi=$data['garansi'];
    $ket_garansi=$data['ket_garansi'];
    $status=$data['status'];
    $pic=$data['pic'];

?>

<html>
<head>
<title>ubah perangkat</title>
</head>
<body>
		<script type="text/javascript">
				function cekid_unit()
				{
                  if(document.getElementById('id_unit').value == '')
				  {  
                   alert('ID Unit tidak boleh kosong');  
                   document.getElementById('id_unit').focus();  
                   return false;  
                  }  
 				}
				
		</script>
	<link type="text/css" href="asset/themes/base/ui.all.css" rel="stylesheet" />   
    <script type="text/javascript" src="asset/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="asset/ui/ui.core.js"></script>
    <script type="text/javascript" src="asset/ui/ui.datepicker.js"></script>   
    <script type="text/javascript" src="asset/ui/i18n/ui.datepicker-id.js"></script>
    <script type="text/javascript" src="asset/ui/effects.core.js"></script>
    <script type="text/javascript" src="asset/ui/effects.drop.js"></script>

    <script type="text/javascript"> 
      $(document).ready(function(){
        $("#periode").datepicker({
          showAnim    : "drop",
          showOptions : { direction: "up" }
        });
      });

	  </script>

<h2>UBAH PERANGKAT</h2>
<hr />
<form method="post" action="master_perangkat_ubah_query.php" style="margin:0;" >
<table border="1" cellspacing="1" cellpadding="1">
 <tr>
    <td bgcolor="#C0C0C0">TIPE </td>
	<td bgcolor="#F0F8FF" >: <?php echo $id_tipe."-".$nama_tipe; ?> 
	 <input type="hidden" name="id_tipe" value="<?php echo $id_tipe; ?>" > </td>
 </tr>
 <tr>
    <td bgcolor="#C0C0C0">GRUP </td>
	<td bgcolor="#F0F8FF" >: <?php echo $id_grup."-".$nama_grup; ?> 
	    <input type="hidden" name="id_grup" value="<?php echo $id_grup; ?>" >  </td>
 </tr>

 <tr>
    <td bgcolor="#C0C0C0">UNIT </td>
	<td bgcolor="#F0F8FF" >: <?php echo $unit; ?> 
	    <input type="hidden" name="unit" value="<?php echo $data['unit']; ?>" >  </td>
 </tr>

  <tr>
    <td bgcolor="#C0C0C0">NAMA UNIT </td>
    <td>:<input  type="text" maxlength="50" size="50" name="nama_unit" value="<?php echo $data['nama_unit']; ?>"></td>
  </tr>
  <tr>
    <td bgcolor="#C0C0C0">PERIODE </td>
	<td>:<input type="date" id="periode" maxlength="10" size="10" name="periode"  value="<?php echo $data['periode']; ?>"> </td>
  </tr>
  <tr>
    <td bgcolor="#C0C0C0">SPEK1 </td>
	<td>:
	<textarea name="spek1" maxlength="255" cols="80" rows="1" value="<?php echo $data['spek1']; ?>"> <?php echo trim($data['spek1']); ?> </textarea required></td>
	</tr>
  <tr>
    <td bgcolor="#C0C0C0">SPEK2 </td>
    <td>:
	<textarea name="spek2" maxlength="255" cols="80" rows="2" value="<?php echo $data['spek2']; ?>"> <?php echo trim($data['spek2']); ?> </textarea required></td>
	</tr>
  <tr>
    <td bgcolor="#C0C0C0">SPEK3 </td>
    <td>:
	<textarea name="spek3" maxlength="255" cols="80" rows="1" value="<?php echo $data['spek3']; ?>"> <?php echo trim($data['spek3']); ?> </textarea required></td>
	</tr>
  <tr>
    <td bgcolor="#C0C0C0">JUMLAH STOK </td>
    <td>:<input type="text" name="jumlah_stok" maxlength="12" size="12" value="<?php echo number_format($data['jumlah_stok']); ?>"></td>
  </tr>
  <tr>
    <td bgcolor="#C0C0C0">HARGA Rp. </td>
    <td>:<input type="text" name="harga" maxlength="12" size="12" value="<?php echo number_format($data['harga']); ?>"></td>
  </tr>
  <tr>
    <td bgcolor="#C0C0C0">HARGA $ </td>
    <td>:<input type="text" name="harga2" maxlength="12" size="12" value="<?php echo number_format($data['harga2']); ?>"></td>
  </tr>
  <tr>
    <td bgcolor="#C0C0C0">GARANSI </td>
    <td>:<input type="text" name="garansi" maxlength="99" size="10"  value="<?php echo number_format($data['garansi']); ?>" > 
       -:<select name="ket_garansi">
	      <option <?php if ($ket_garansi=='Tahun'){echo "selected";}?> value='Tahun'>Tahun</option>
	      <option <?php if ($ket_garansi=='Bulan'){echo "selected";}?> value='Bulan'>Bulan</option>
	</td>
  </tr>

  <tr>
    <td bgcolor="#C0C0C0">PIC </td>
   <td>:<select name="pic">
    <?php
    $query=("select pic,nama_pic from master_pic where pic like '%' and divisi='IT' and status='Y' ");
    $query_hasil=mysql_query($query);
    while($r=mysql_fetch_array($query_hasil))
    {
    if ($data[pic] == $r[pic])
    {
    echo "<option selected='selected' value=$r[pic]>$r[pic].$r[nama_pic]</option>";
    }
    else
    {
    echo "<option value=$r[pic]>$r[pic].$r[nama_pic]</option>";
     }
  	 }
     ?>
	</select>
	</td>
  </tr>
  <tr>
    <td bgcolor="#C0C0C0">STATUS</td>
    <td>:<select name="status">
	<option <?php if ($status=='Y'){echo "selected";}?> value='Y'>Y</option>
	<option <?php if ($status=='N'){echo "selected";}?> value='N'>N</option>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" value="SIMPAN">
    <input type="button" value="BATAL" onclick="self.history.back();"></td>
  </tr>
</table>

</form>
</body>
</html>
